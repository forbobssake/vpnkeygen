#!/usr/bin/env bash 
DEBUG=0
set -o errexit # script exit when a command fails and add || true to commands that you allow to fail.
set -o nounset # exit when script tries to use undeclared variables.
#set -o xtrace  # trace what gets executed. Useful for debugging.

#########################################################
# 						TODO							#
#########################################################
# consider creation of CCD/FILES
# Maybe do config file and parse with awk for easy of setting ARCHIVE_FILE
# SWITCHES and USAGE needs work
# encapsule ECHO infomation in $DEBUG 
# move client files to ${tmpdir}/{KEY_NAME}/ so only the config are toplevel in the client archives, tidy up.

#check for root privileges
if [[ $EUID -ne 0 ]]; then
	echo "The script must be run as root" 1>&2
	exit 1
fi

# Initialize our own variables

#find out where we are launching from
RUN_DIR=$( cd "$( dirname "$0" )" && pwd )
#DO NOT CHANGE THIS, USE T_FLAG BELOW
TEMPLATE_FILE=${RUN_DIR}/TEMPLATE.conf
OPENVPN_CONF=/etc/openvpn/openvpn.conf

# BELOW HERE VAR=0 MEANS LOAD FROM CONFIG

#set to either server IP
SERVER_IP=0
#set to either server PORT
SERVER_PORT=0
#set to /path/to/server_ca.crt
SERVER_CA=0
#set server PROTOCOL udp or tcp
SERVER_PROTO=0
#set server DEV tun or tap
SERVER_DEV=0
#set /path/to/TEMPLATE
T_FLAG=0
#set custom path to EASY_RSA
P_FLAG=0
#override default key strenght
K_FLAG=0
#use tar instead of default 7zip
USE_TAR=0

# Usage info
show_help_long() {
cat << EOF
Usage: ${0##*/} [-hvT] [-t TEMPLATE] [-P EASY_RSA_PATH] [-i IP] [-p PORT] [-s SERVER_CA] [-q PROTOCOL] [-e DEV] [-a PATH] [-k SIZE] -c VPN_SERV_CFG CLIENTNAME

Automate openvpn client keys and package them into a simple archive with
appropriate openvpn configuration

	-h          	 display this help and exit
	-c VPN_SERV_CFG	 load configuration from CONFIG file (defaults to /etc/openvpn/openvpn.conf)
	-t TEMPLATE      load template from TEMPLATE file (defaults to ./TEMPLATE.ovpn)
	-P EASY_RSA_PATH path of easy-rsa diretory (defaults to /etc/openvpn/easy-rsa)
	-v               verbose reporting (DEBUG)
	-i SERVER_IP     use SERVER_IP instead for autodectecting
	-p SERVER_PORT   use SERVER_PORT instead for client configuration file
	-s SERVER_CA	 specify SERVER_CA.crt file 
	-q SERVER_PROTO  use SERVER_PROTO, must be either udp or tcp
	-e SERVER_DEV    use SERVER_DEV, must be either tun or tap
	-T               use tar.gz instead of default 7zip archive
	-a ARCHIVE_PATH  use ARCHIVE_PATH for storage (default: /etc/openvpn/easy-rsa/keys)
	-k KEY_SIZE      set key strenght to KEY_SIZE

EOF
}
show_help_short() {
	echo "Usage: ${0##*/} [-hvT] [-t TEMPLATE] [-P EASY_RSA_PATH] [-i IP] [-p PORT] [-s SERVER_CA] [-q PROTOCOL] [-e DEV] [-a PATH] [-k SIZE] -c VPN_SERV_CFG CLIENTNAME" 1>&2
}     

#Clear easy-rsa variables before we start and set a few defaults
EASY_RSA=/etc/openvpn/easy-rsa
OPENSSL=""
PKCS11TOOL=""
GREP=""
KEY_CONFIG=""
KEY_DIR=/etc/openvpn/easy-rsa/keys/
PKCS11_MODULE_PATH=""
PKCS11_PIN=""
KEY_SIZE=""
CA_EXPIRE=""
KEY_EXPIRE=""
KEY_COUNTRY=""
KEY_PROVINCE=""
KEY_CITY=""
KEY_ORG=""
KEY_EMAIL=""
KEY_CN=""
KEY_NAME=""
KEY_OU=""
PKCS11_MODULE_PATH=""
PKCS11_PIN=""

#FML can't move this unless i move var reset
ARCHIVE_PATH=${KEY_DIR}

OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts ":hvTc:t:i:p:s:q:e:P:a:k:" opt; do
    case "$opt" in
        h)
            show_help_long
            exit 0
            ;;
        c)	OPENVPN_CONF=$OPTARG
			;;
		t)  T_FLAG=$OPTARG
            ;;
        P)  P_FLAG=$OPTARG
            ;;
        v)  echo "(v)erbose reporting"
            set -o xtrace
            ;;
        i)  SERVER_IP=$OPTARG
			;;
        p)  SERVER_PORT=$OPTARG
			;;
		s)	SERVER_CA=$OPTARG #Must be full path to server_ca.crt
			;;
		q)	SERVER_PROTO=$OPTARG
			;;
		e)	SERVER_DEV=$OPTARG
			;;
		T)	USE_TAR=1
			;;
		a)	ARCHIVE_PATH=$OPTARG
			;;
		k)	K_FLAG=$OPTARG
			;;
        *)
            echo "ILLEGAL OPTION!"
			show_help_short
            exit 1
            ;;
    esac
done
shift "$((OPTIND-1))" # Shift off the options and optional --.

#check that $CLIENT_NAME has be supplied after running getopts, otherwise print usage and exit
[ "$#" -eq 1 ] || {
	echo "script requires CLIENTNAME argument to run"
	show_help_short
	exit 1
}

#require openvpn.conf to be a valid file otherwise have user specify a valid one using -c
if [ ! -f "$OPENVPN_CONF" ]; then
	#throw error if OPENVPN_CONF is not a valid file
	echo "error OPENVPN_CONF not found: $OPENVPN_CONF, please use -c path/to/config" 1>&2
	exit 1
fi
#check if user has supplied SERVER_IP using switch
if [ $SERVER_IP = 0 ]; then
	#if no, get external ip
	if SERVER_IP="`wget -O - -o /dev/null http://smex.dk/ip.php`"; then
		echo "FOUND EXTERNAL IP: $SERVER_IP"
	else
		echo "error could not find server external ip please use -s IP"
		exit 1
	fi
else
	echo "USING COMMANDLINE SERVER_IP: $SERVER_IP"
fi
#check if user has supplied SERVER_PORT using switch
if [ $SERVER_PORT = 0 ]; then
	#if no, then parse OPENVPN_CONF 
	if SERVER_PORT="$(awk '{ if( $1 !~ "^#") } /^port/ {f=1;print $2} /^Match/{exit} END{exit !f}' ${OPENVPN_CONF})"; then
		echo "USING OPENVPN_CONF IP: $SERVER_PORT"
	else
		echo "error could not find server external ip please use -s IP"
	fi
else
	echo "USING COMMANDLINE SERVER_PORT: $SERVER_PORT"
fi
#check if user has supplied SERVER_CA using switch
if [ $SERVER_CA = 0 ]; then
	#if no, then parse using OPENVPN_CONF
	if SERVER_CA="$(awk '{ if( $1 !~ "^#") } /^ca / {f=1;print $2} /^Match/{exit} END{exit !f}' ${OPENVPN_CONF})"; then
		#We also want to make sure it is in fact a valid file
		if [ -f $SERVER_CA ]; then
			echo "USING OPENVPN_CONF CA: $SERVER_CA"
		else
			echo "Unable to read file $SERVER_CA"
			exit 1
		fi
	else
		echo "failed to find SERVER_CA defined in $OPENVPN_CONF"
	fi
elif [ ! -f $SERVER_CA ]; then
	echo "Unable to read file $SERVER_CA"
	exit 1
else
	echo "USING COMMANDLINE SERVER_CA: $SERVER_CA"	
fi
#check if user has supplied SERVER_PROTO using switch
if [ $SERVER_PROTO = 0 ]; then
	#if no, then parse using OPENVPN_CONF
	if ! SERVER_PROTO="$(awk '{ if( $1 !~ "^#") } /^proto / {f=1;print $2} /^Match/{exit} END{exit !f}' ${OPENVPN_CONF})"; then
			echo "failed to find SERVER_PROTO defined in $OPENVPN_CONF"
			exit 1
	#lets make sure the value is valid		
	elif [[ ! $SERVER_PROTO =~ (t|T|u|U)(c|C|d|D)(p|P) ]]; then
			echo "SERVER_PROTO in $OPENVPN_CONF must be either 'tcp' or 'udp'"
			exit 1
	else
		echo "USING OPENVPN_CONF PROTO: $SERVER_PROTO"
		
	fi
#lets make sure the value is valid	
elif [[ ! $SERVER_PROTO =~ (t|T|u|U)(c|C|d|D)(p|P) ]]; then
	echo "echo SERVER_PROTO must be either tcp or udp"
	exit 1
else
	echo "USING COMMANDLINE SERVER_PROTO: $SERVER_PROTO"	
fi
#check if user has supplied SERVER_PROTO using switch
if [ $SERVER_DEV = 0 ]; then
	#if no, then parse using OPENVPN_CONF
	if ! SERVER_DEV="$(awk '{ if( $1 !~ "^#") } /^dev/ {f=1;print $2} /^Match/{exit} END{exit !f}' ${OPENVPN_CONF})"; then
			echo "failed to find SERVER_DEV defined in $OPENVPN_CONFF"
			exit 1
	#lets make sure the value is valid		
	elif [[ ! $SERVER_DEV =~ (t|T)(u|U|a|A)(p|P|n|N) ]]; then
			echo "SERVER_DEV in $OPENVPN_CONF must be either 'tun' or 'tap'"
			exit 1
	else
		echo "USING OPENVPN_CONF DEV: $SERVER_DEV"
		
	fi
#lets make sure the value is valid	
elif [[ ! $SERVER_DEV =~ (t|T)(u|U|a|A)(p|P|n|N) ]]; then
	echo "SERVER_DEV must be either 'tun' or 'tap'"
	exit 1
else
	echo "USING COMMANDLINE SERVER_DEV: $SERVER_DEV"	
fi
#We need a valid TEMPLATE_FILE, so check if switch has been used
if [ $T_FLAG = 0 ]; then
	#if no, we check default check that TEMPLATE_FILE exists
	if [ ! -f "$TEMPLATE_FILE" ]; then
		echo "error TEMPLATE_FILE not found: $TEMPLATE_FILE, please specify using -t /path/to/file" 1>&2
		exit 1
	else
		echo "USING TEMPLATE_FILE: $TEMPLATE_FILE"
	fi
elif [ ! $T_FLAG = 0 ] && [ -f "$T_FLAG" ]; then
	TEMPLATE_FILE=$T_FLAG
	echo "USING COMMANDLINE TEMPLATE_FILE: $TEMPLATE_FILE"
	exit 0
else
	echo "error TEMPLATE_FILE not found: $T_FLAG, please specify using -t /path/to/file" 1>&2
	exit 1
fi

#if no -P switch check default $EASY_RSA is a dir. otherwise die
if [ $P_FLAG = 0 ] && [ ! -d $EASY_RSA ]; then
	echo "ERROR: can not find default EASY_RSA directory (${EASY_RSA})" 1>&2
	exit 1
elif [ ! $P_FLAG = 0 ] && [ ! -d "$P_FLAG" ]; then
	echo "ERROR: can not find -P EASY_RSA directory (${P_FLAG})" 1>&2
	exit 1
#if -P switch check that it is a valid dir. and export it to $EASY_RSA
elif [ ! $P_FLAG = 0 ] && [ -d "$P_FLAG" ]; then
	EASY_RSA="$P_FLAG"
	echo "USING COMMANDLINE EASY_RSA: $EASY_RSA"
else
	echo "USING DEFAULT EASY_RSA: $EASY_RSA"
fi

#check that $EASY_RSA/vars is a valid file
if [ ! -f "${EASY_RSA}/vars" ]; then
	echo "ERROR: can not find vars file (${EASY_RSA}/vars)" 1>&2
	exit 1
#If it is, lets load the file
else
	cd ${EASY_RSA}
	. vars >/dev/null
	cd $OLDPWD
	echo "LOADED VARS FROM: ${EASY_RSA}/vars"
fi
if [ ! -d $ARCHIVE_PATH ]; then
	echo "can not find ARCHIVE_PATH: $ARCHIVE_PATH"
	exit 1
fi


CLIENT_NAME=$1

#########################################################
#					Key creation						#
#########################################################

#check for key, if not found generate it
if [ ! -e ${KEY_DIR}/${CLIENT_NAME}.key ]; then
	if [ ! $K_FLAG = 0 ]; then
		KEY_SIZE=$K_FLAG
		echo "USING COMMANDLINE KEY_SIZE: $KEY_SIZE"
	fi
	#Fix KEY_CN when calling pkitool without --interactive
	export KEY_CN=$CLIENT_NAME
	${EASY_RSA}/pkitool $CLIENT_NAME 
#	./build-dh
else
	echo "NOT BUILDING KEY, EXISTS ALREADY: ${KEY_DIR}/${CLIENT_NAME}.key"
fi

###########################################################
#						Key packaging					  #
###########################################################
if [ "$USE_TAR" == 1 ]; then
	ARCHIVE_FILE=${ARCHIVE_PATH}/${CLIENT_NAME}.tar.gz
else
	ARCHIVE_FILE=${ARCHIVE_PATH}/${CLIENT_NAME}.7z
fi

if [ ! -e ${ARCHIVE_FILE} ]; then
	#set tmpdir
	tmpdir=/tmp/client-openvpn.$$
	#create tmpdir
	mkdir $tmpdir
	#copy template to tmpdir}
	cp ${TEMPLATE_FILE} ${tmpdir}/${CLIENT_NAME}.ovpn
	#fill placeholders with content
	sed -i s/CLIENT/${CLIENT_NAME}/ ${tmpdir}/${CLIENT_NAME}.ovpn
	sed -i s/SERVER_ROOT/${KEY_NAME}_server_ca/ ${tmpdir}/${CLIENT_NAME}.ovpn
	sed -i s/SERVER_IP/${SERVER_IP}/ ${tmpdir}/${CLIENT_NAME}.ovpn
	sed -i s/SERVER_PORT/${SERVER_PORT}/ ${tmpdir}/${CLIENT_NAME}.ovpn
	sed -i s/SERVER_DEV/${SERVER_DEV}/ ${tmpdir}/${CLIENT_NAME}.ovpn
	sed -i s/SERVER_PROTO/${SERVER_PROTO}/ ${tmpdir}/${CLIENT_NAME}.ovpn
	#copy filled .ovpn file to .conf for linux
	cp ${tmpdir}/${CLIENT_NAME}.ovpn ${tmpdir}/${CLIENT_NAME}.conf
	#copy server_ca and client key files
	cp $SERVER_CA ${tmpdir}/${KEY_NAME}_server_ca.crt
	cp ${KEY_DIR}/${CLIENT_NAME}.key ${tmpdir}
	cp ${KEY_DIR}/${CLIENT_NAME}.crt ${tmpdir}
	if [ "$USE_TAR" == 1 ]; then
		cd "$tmpdir"
		tar zcvf ${ARCHIVE_FILE} .
		cd $OLDPWD
	else
		cd "$tmpdir"
		7z a ${ARCHIVE_FILE} .
		cd $OLDPWD
	fi
	rm -rf $tmpdir
else
	echo "Nothing to do, so nothing done. (${ARCHIVE_FILE} already exsists)"
fi
