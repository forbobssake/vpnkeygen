Usage: ${0##*/} [-hvT] [-t TEMPLATE] [-P EASY_RSA_PATH] [-i IP] [-p PORT] [-s SERVER_CA] [-q PROTOCOL] [-e DEV] [-a PATH] [-k SIZE] -c VPN_SERV_CFG CLIENTNAME

Automate openvpn client keys and package them into a simple archive with
appropriate openvpn configuration

    -h          	 display this help and exit
	-c VPN_SERV_CFG	 load configuration from CONFIG file (defaults to /etc/openvpn/openvpn.conf)
	-t TEMPLATE		 load template from TEMPLATE file (defaults to ./TEMPLATE.ovpn)
	-P EASY_RSA_PATH path of easy-rsa diretory (defaults to /etc/openvpn/easy-rsa)
    -v               verbose reporting (DEBUG)
    -i SERVER_IP     use SERVER_IP instead for autodectecting
	-p SERVER_PORT   use SERVER_PORT instead for client configuration file
	-s SERVER_CA	 specify SERVER_CA.crt file 
	-q SERVER_PROTO  use SERVER_PROTO, must be either udp or tcp
    -e SERVER_DEV    use SERVER_DEV, must be either tun or tap
    -T               use tar.gz instead of default 7zip archive
    -a ARCHIVE_PATH  use ARCHIVE_PATH for storage (default: /etc/openvpn/easy-rsa/keys)
	-k KEY_SIZE      set key strenght to KEY_SIZE
